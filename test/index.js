var colors = [
    'red',
    'green',
    'blue',
    'yellow',
    'magenta',
    'pink'
];

window.onload = function () {
    document.addEventListener('click', function (evt) {
        if (evt.target.nodeName.toLowerCase() === 'div') {
            processDivClick(evt.target, colors);
        }
    });

    function processDivClick(element, colors) {

        if (element.nodeName.toLowerCase() == 'div') {
            var style = Math.random() * colors.length;
            style = Math.floor(style);

            element.style.backgroundColor = colors[style];
        }

        var div1 = getComputedStyle(document.querySelector('a+div'));
        var style1 = div1.backgroundColor;
        var div2 = getComputedStyle(document.querySelector('a+div>div'));
        var style2 = div2.backgroundColor;
        var div3 = getComputedStyle(document.querySelector('a+div>div>div'));
        var style3 = div3.backgroundColor;

        if (style1 == style2 && style1 == style3 && style3 == style2) {
            setTimeout(function () {
                alert('Готово!')
            }, 500);
        }
    }
};