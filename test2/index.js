var colors = [
    'red',
    'green',
    'blue',
    'yellow',
    'magenta',
    'pink'
];

window.onload = function () {
    var count = prompt('Сколько нужно квадратов?', 3);
    var startWidth = 50 * count;

    for (var i = 1; i <= count; i++) {

        var col = Math.random() * colors.length;
        col = Math.floor(col);
        var color = colors[col];

        var div = document.createElement('div');
        div.className = "el" + i;

        if (i == 1) {
            var styleDiv = "background-color: " + color + "; \
            width: " + startWidth + "px; \
            height: " + startWidth + "px; \
            position: relative; \
            name: " + color + "; \
            ";
            div.style.cssText = styleDiv;
            document.body.appendChild(div);
        } else {
            var styleDiv = "background-color: " + color + "; \
            width: " + (startWidth - 50 * (i - 1)) + "px; \
            height: " + (startWidth - 50 * (i - 1)) + "px; \
            position: absolute; \
            top: 25px; \
            left: 25px; \
            name: " + color + "; \
            ";

            div.style.cssText = styleDiv;
            var parentEl = document.querySelector('div.el' + (i - 1));
            parentEl.appendChild(div);
        }
    }

    var timer = setInterval(function () {
        var randomNum = (1 + Math.random() * count);
        randomNum = Math.floor(randomNum);

        var targetDiv = document.querySelector('div.el' + randomNum);
        var color = Math.random() * colors.length;
        col = Math.floor(color);

        targetDiv.style.backgroundColor = colors[col];
        targetDiv.setAttribute('name', colors[col]);

        var final = document.querySelectorAll('div[name="' + colors[col] + '"]');

        if (final.length == count) {
            clearInterval(timer);
        }
    }, 500);

};